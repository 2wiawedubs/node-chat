import { Response, Request, NextFunction } from "express";
import MessageService from "../services/MessageService";
import RoomService from "../services/RoomService";
import mongoose from "mongoose";
import UserService from "../services/UserService";
import dotenv from 'dotenv';

dotenv.config()

const ObjectId = mongoose.Types.ObjectId;
const ROOMS_NUM = process.env.ROOMS_NUM || 2;
export default class ChatController {

    static async storeMessage(room: any, user: any, text: any) {
        // const { room, user, text } = req.body;

        const roomObj = await RoomService.showByRoom(room)
        const userObj = await UserService.showByName(user)
        if (!roomObj) {
            RoomService.create({ name: room })
        }
        // @ts-ignore
        await MessageService.create({ room: ObjectId(roomObj._id), user: ObjectId(userObj._id), text })
            .then(message => {
                return message;
                // res.status(200).json(message)
                // next()
            })
            .catch(err => {
                console.error(err);
            })
    }

    static async getMessageTextsFromRoom(req: Request, res: Response, next: NextFunction) {
        const { room } = req.params;

        const roomObj = await RoomService.showByRoom(room)

        const resMessages = await MessageService.listByRoom(roomObj)
            .then(messages => {
                return messages
            })
            .catch(err => {
                console.error(err);
            })

        req.body.messages = []

        // @ts-ignore
        for (const message of resMessages) {
            const { name: userName } = await UserService.show(message.user)
            const { name: roomName } = await RoomService.show(message.room)
            const time = `${new Date(message.createdAt).toDateString()} ${new Date(message.createdAt).toLocaleTimeString()}`

            req.body.messages.push({
                user: userName,
                room: roomName,
                text: message.text,
                createdAt: time
            })
        }

        next()
    }

    static renderChat(req: Request, res: Response, next: NextFunction) {
        res.render("chat", {
            userName: req.session.userName,
            id: req.params.id,
            layout: "main-nav"
        })
    }

    static renderChatList(req: Request, res: Response, next: NextFunction) {
        res.render("chat-list", {
            layout: "main-nav",
            userName: req.session.userName,
            rooms_num: ROOMS_NUM
        })
    }

    static renderHistory(req: Request, res: Response, next: NextFunction) {
        res.render("history", {
            room: req.params.room,
            messages: req.body.messages,
            userName: req.session.userName,
            layout: "main-nav"
        })
    }

    static renderHistoryList(req: Request, res: Response, next: NextFunction) {
        res.render("history-list", {
            layout: "main-nav",
            userName: req.session.userName,
            rooms_num: ROOMS_NUM
        })
    }

}