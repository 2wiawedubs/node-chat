import { Response, Request, NextFunction } from "express";
import session from "express-session";
// import { Cookie } from "express-session";
import UserService from "../services/UserService";

export default class UserController {
    static async register(req: Request, res: Response) {
        const newUser = await UserService.create(req.body)
            .then(user => {
                res.redirect("/user/login")
            })
            .catch(err => {
                res.redirect("/user/register?error=true")
                console.log(err);

            })

    }

    static async index(req: Request, res: Response) {
        try {
            const allUsers = await UserService.index()

            return res.status(200).json(allUsers)
        } catch (err) {
            console.error(err);
        }
    }

    static async show(req: Request, res: Response) {
        try {
            const user = await UserService.show(req.params.id)

            return res.status(200).json(user)
        } catch (err) {
            console.error(err);
        }
    }

    static async update(req: Request, res: Response) {
        try {
            const updateUser = await UserService.update(req.params.id, req.body.name, req.body.email, req.body.password)

            return res.status(200).json(updateUser)
        } catch (err) {
            console.error(err);
        }
    }

    static async delete(req: Request, res: Response) {
        try {
            const deleteUser = await UserService.delete(req.params.id)

            return res.status(200).json(deleteUser)
        } catch (err) {
            console.error(err);
        }
    }

    static async login(req: Request, res: Response, next: NextFunction) {
        await UserService.exist(req.body.username, req.body.password)
            .then(exist => {
                if (exist) {
                    res.cookie("user", `{"name":"${req.body.username}", "email":"${req.body.password}"}`, { encode: JSON.stringify })
                    req.session.userName = req.body.username
                    next()
                } else {
                    res.redirect(`${req.url}?error=User not found`)
                }
            })
            .catch((err: Error) => {
                console.log(err);
            })
    }

    static isLogged(req: Request, res: Response, next: NextFunction) {
        if (req.session.userName) {
            next()
        } else {
            res.redirect("/user/login")
        }
    }

    static logout(req: Request, res: Response) {
        req.session.destroy(err => {
            if (err) {
                console.log(err);
            }
        })
        res.redirect("/")
    }


    static renderChatList(req: Request, res: Response) {
        if (req.baseUrl === "/chat/list")
            res.render("chat-list", { layout: "main-nav", userName: req.session.userName })
        else
            res.redirect("/chat/list")
    }

    static renderLogin(req: Request, res: Response) {
        res.render("login", { error: req.query.error })
    }

}


