import mongoose from "mongoose";
const Schema = mongoose.Schema;

const Room = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    messages: [{
        type: Schema.Types.ObjectId,
        ref: "Message",
    }]
});

export default mongoose.model("Room", Room);