import mongoose, {Query, Document, Model, model} from "mongoose";
const Schema = mongoose.Schema;

export interface IMessage {
    room: {type: mongoose.Schema.Types.ObjectId, required: true};
    user: {type: mongoose.Schema.Types.ObjectId, required: true};
    text: string;
    createdAt: Date;
}

interface IMessageQueryHelpers {
    byRoom(room: mongoose.Types.ObjectId): Query<any, Document<IMessage>> & IMessageQueryHelpers;
}

const schema = new Schema<IMessage>({
    room: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Room",
        required: true,
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: true,
    },
    text: {
        type: String,
        required: true,
    },
    createdAt: {
        type: Date,
        default: Date.now,
    },
});

// schema.query.byRoom = function(room: mongoose.Types.ObjectId): Query<any, Document<IMessage>> & IMessageQueryHelpers {
//     return this.find({name: room})
// }

const MessageModel = mongoose.model<IMessage, Model<IMessage, IMessageQueryHelpers>>("Message", schema);

export default model<IMessage>("Message", schema);

