import MessageService from "./services/MessageService";
import RoomService from "./services/RoomService";

const ROOMS_NUM = process.env.ROOMS_NUM || 2;


for (let index = 1; index <= ROOMS_NUM; index++) {
    RoomService.showByRoom(index.toString())
        .then(room => {
            if (!room) {
                RoomService.create({ name: index.toString() })
            }
        })

}
