import express, { NextFunction, Request, Response } from 'express';
import ChatController from '../controllers/ChatController';
const router = express.Router();
import UserController from '../controllers/UserController';

router.route('/')
    .get((req: Request, res: Response) => {
        res.render('index', { error: req.query.error })
    })

router.route("/user/register")
    .post(UserController.register)
    .get((req: Request, res: Response) => {
        res.render('register', { error: req.query.error })
        })

router.route('/user/login')
    .get((req: Request, res: Response) => {
        res.render('login', { error: req.query.error })
    })
    .post(UserController.login, UserController.renderChatList)

router.route('/user/logout')
    .get(UserController.logout)

router.route("/chat/list")
    .all(UserController.isLogged)
    .get(ChatController.renderChatList)

router.route("/chat/view/:id")
    .all(UserController.isLogged)
    .get(ChatController.renderChat)

router.route("/history/list")
    .all(UserController.isLogged)
    .get(ChatController.renderHistoryList)


router.route("/history/view/:room")
    .all(UserController.isLogged)
    .get(ChatController.getMessageTextsFromRoom, ChatController.renderHistory)

export default router