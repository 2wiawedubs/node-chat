import Handlebars from 'handlebars';
Handlebars.registerHelper("times_loop", (n, block) => {
    let accum = "";
    for (let i = 1; i <= n; ++i)
        accum += block.fn(i);
    return accum;
})