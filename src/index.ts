import express, { Request } from 'express';
import { createServer } from 'http';
import mongoose from 'mongoose';
import dotenv from 'dotenv';
import routes from './routes/routes';
import { env } from 'process';
import { engine } from 'express-handlebars';
import path from 'path';
import session from 'express-session';
import cookieParser from 'cookie-parser';

dotenv.config()

const app = express()
const PORT = env.PORT
const MONGO_DB = env.MONGO_DB

const publicDir = path.join(__dirname, '../public')



app.engine("handlebars", engine())

app.set("view engine", "handlebars")
app.set("views", path.join(publicDir, "/views"))

app.use("/public", express.static(publicDir))
app.use(express.urlencoded({ extended: true }))
app.use(cookieParser())
app.use(express.json())
app.use(session({
   secret: 'keyboard cat',
   resave: false,
   saveUninitialized: true,
   cookie: { /* secure: true, */ maxAge: 24*60*60*1000 }
 }))


console.log(app.get("views"))

app.use("/", routes)

const server = createServer(app)

require("./deploy")
require("./handlebars")

server.listen(PORT, () => {
   console.log(`Example app listening on port ${PORT}`);
   mongoose.connect(
      MONGO_DB,
      // @ts-ignore
      { useUnifiedTopology: true, useNewUrlParser: true },
   )
      .then(res => {
         console.log(`Succesfully conected to database: ${res.connection.name}`);
      })
      .catch(err => {
         console.error(err);
      })
      require("./io")
})
