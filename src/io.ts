import { Server } from 'socket.io';
import { createServer } from 'http';
import dotenv from 'dotenv';
import { env } from 'process';
import ChatController from './controllers/ChatController';
import UserController from './controllers/UserController';
import express, { NextFunction, Request, Response } from 'express';

dotenv.config()

const SOCKET_PORT = env.SOCKET_PORT
const PORT = env.PORT

const usersRooms = new Map<string, string>()

const socketServer = createServer();

socketServer.listen(SOCKET_PORT, () => {
   console.log(`Socket server listening on port ${SOCKET_PORT}`)
});

const io = new Server(socketServer, {
   cors: {
      origin: `http://localhost:${PORT}`,
   }
});

io.on("connection", (socket) => {

   socket.on("room", (_data) => {
      socket.join(_data.room);
      usersRooms.set(socket.id, _data.room)
   });

   socket.on(`message`, _data => {
      try {
         const data = {
            "userName": _data.user.name,
            "message": _data.message,
         }
         ChatController.storeMessage(usersRooms.get(socket.id), data.userName, data.message)
         socket.to(usersRooms.get(socket.id)).emit("message", data)

      } catch (error) {
         console.error(`${socket.id} must be logged in to send a message`)
         socket.emit("exception", {message: "You must be logged in to send a message"})
      }

   })

   socket.on("disconnect", () => {
      console.log(socket.id, " disconnected")
   })


})