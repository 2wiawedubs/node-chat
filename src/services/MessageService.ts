import mongoose from "mongoose";
import { Document, Model, Query, connect, model } from 'mongoose';
import { IMessage } from "../models/MessageModel";

const Schema = mongoose.Schema;
import Message from "../models/MessageModel";
import RoomService from "./RoomService";

export default class MessageService {
    static async list() {
        return Message.find()
            .then(messages => {
                return messages;
            })
            .catch(err => {
                console.error(err);
            })
    }

    static async listByRoom(room: mongoose.Types.ObjectId){
        return Message.find({ room })
            .then(messages => {
                return messages;
            })
            .catch(err => {
                console.error(err);
            })
    }

    static async show(id: any) {
        return Message.findById(id)
            .then(message => {
                return message;
            })
            .catch(err => {
                console.error(err);
            })
    }

    static async create(data: any) {
        const { room, user, text } = data;

        return Message.create({ room, user, text })
            .then(message => {
                return message;
            })
            .catch(err => {
                console.error(err);
            })
    }

    static async update(id: string, text: string) {
        return Message.findByIdAndUpdate(id, { text }, { new: true })
            .then(message => {
                return message;
            })
            .catch(err => {
                console.error(err);
            })
    }

    static async delete(id: string) {
        return Message.findByIdAndDelete(id)
            .then(message => {
                return message;
            })
            .catch(err => {
                console.error(err);
            })
    }
}
