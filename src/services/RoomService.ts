import { Response, Request } from "express";
import mongoose from "mongoose";
import Room from "../models/RoomModel";

// Room Service
export default class RoomService {

    static async index() {
        return Room.find()
            .then(rooms => {
                return rooms;
            })
            .catch(err => {
                console.error(err);
            })
    }

    static async show(id: any) {
        return Room.findById(id)
        .then(room => {
            return room;
        })
        .catch(err => {
            console.error(err);
        })
    }

    static async showByRoom(_room: string) {
        return Room.findOne({ "name": _room })
            .then(room => {
                return room;
            })
            .catch(err => {
                console.error(err);
            })
    }

    static async create(data: { name: string }) {
        const { name } = data;

        return Room.create({ name })
            .then(room => {
                return room;
            })
            .catch(err => {
                console.error(err);
            })

    }

    static async update(id: string, name: string) {
        return Room.findByIdAndUpdate(id, { name }, { new: true })
            .then(room => {
                return room;
            })
            .catch(err => {
                console.error(err);
            })
    }

    static async delete(id: string) {
        return Room.findByIdAndDelete(id)
            .then(room => {
                return room;
            })
            .catch(err => {
                console.error(err);
            })
    }
}