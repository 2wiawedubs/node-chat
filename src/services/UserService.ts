import { Response, Request } from "express";
import User from '../models/UserModel'
import mongoose from "mongoose";
import bcrypt from "bcrypt"

// User Service
export default class UserService {

    static async index() {
        try {
            const allUsers = await User.find();

            return allUsers;
        } catch (err) {
            console.error(err);
        }
    }

    static async show(id: any) {
        try {
            const user = await User.findById(id);

            return user;
        } catch (err) {
            console.error(err);
        }
    }

    static async showByName(_username: string) {
        return User.findOne({ name: _username })
            .then(user => {
                return user;
            })
            .catch(err => {
                console.error(err);
            })
    }

    static async create(data: any) {
        const { name, email, password } = data;

        const salt = await bcrypt.genSalt(10);
        const hashedPassword = await bcrypt.hash(password, salt);


        try {
            const newUser = {
                name: data.name,
                email: data.email,
                password: hashedPassword,
            }

            const user = await User.create(newUser);

            return user;

        } catch (err) {
            console.error(err);
            return err
        }
    }

    static async update(id: string, name: string, email: string, password: string) {
        try {
            const updateUser = await User.findByIdAndUpdate(id, {
                name,
                email,
                password,
            }, { new: true })

            return updateUser;

        } catch (err) {
            console.error(err);
        }
    }

    static async delete(id: string) {
        try {
            const deleteUser = await User.findByIdAndDelete(id)
            return deleteUser
        } catch (err) {
            console.error(err)
        }
    }

    static async exist(_username: string, _password: string): Promise<boolean> {
        let exist = false
        const resUser = await User.findOne({ name: _username})
            .then(user => {
                return user
            })
            .catch(err => {
                console.error(err);
            })
        if (resUser) {
            const validPassword = await bcrypt.compare(_password, resUser.password)
            if (validPassword) {
                exist = true
            }
        }
        return exist
    }
}