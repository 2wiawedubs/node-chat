var socket = io.connect('http://localhost:3001', { 'forceNew': true });

socket.on("exception", function (data) {
  if (data.message = "You must be logged in to send a message")
    window.location.href = '/'; 
  console.log("error");
})

socket.on('connect', function () {
  socket.emit("room", { room: window.location.href.split('/')[5] });
  console.log('Connected')
})

socket.on("message", data => {
  console.log(data);
  $('#messages').append(`<li class="list-group-item border-0"><b>${data.userName}</b>: ${data.message}</li>`)
})


$(document).ready(function () {
  $('#chat_form').submit(function (e) {
    e.preventDefault()
    cookies = document.cookie.split(';')
    data = {}

    cookies.forEach(item => {
      if (item.split('=')[0].trim() == 'user') {
        let user = item.split('=')[1]
        user = JSON.parse(user)

        data.user = JSON.parse(user)
      }
    });
    data.message = $('#message_input').val()
    console.log(data);
    socket.emit(`message`, data)

    $('#messages').append(`<li class="list-group-item d-flex justify-content-end border-0"><b>You</b>: ${data.message}</li>`)

    $('#message_input').val('')
  })
})